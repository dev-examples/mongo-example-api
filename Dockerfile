FROM node:12-alpine3.11 as dependencies
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install --production

FROM node:12-alpine3.11 as builder
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install
COPY . .
RUN yarn build

FROM node:12-alpine3.11
WORKDIR /app
COPY package.json yarn.lock ./
COPY --from=dependencies /app/node_modules ./node_modules
COPY --from=builder /app/dist ./dist
CMD node .
