# mongo example api

## Development

Requires yarn and Docker. 

To installed dependencies:
`yarn install`

To start API server in development mode:
`yarn dev`

## Demo

To run demo in docker:
`docker-compose up`
