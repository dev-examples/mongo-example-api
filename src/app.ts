import express, { Request, Response, NextFunction } from 'express'
// import path  from 'path';
import logger from 'morgan'
import { ResponseError } from './shared/types/errors'
import indexRouter from './routes/root'
import { createDeploymentsRouter } from './routes/deployments/router'
import { createConnection } from './database/database'
import { createDeploymentsDao } from './database/data-access-objects/deployments-dao'

async function createApp() {
  const app = express()
  const connection = await createConnection({ url: process.env.MONGO_URL || '' })

  app.use(logger('dev'))
  app.use(express.json())

  app.use('/', indexRouter)

  const deploymentsDao = await createDeploymentsDao({ connection })
  const deploymentsRouter = createDeploymentsRouter({ deploymentsDao })
  app.use('/api/deployments', deploymentsRouter)

  // error handler
  app.use((err: ResponseError, req: Request, res: Response, next: NextFunction) => {
    // error details only in dev mode
    const details = req.app.get('env') === 'development' ? err : {}

    res.status(err.status || 500)
    res.json({ error: err.message, details })
  })
  return { app, connection }
}

export { createApp }
