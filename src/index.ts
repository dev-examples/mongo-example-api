// tslint:disable: no-console
import { createApp } from './app'
import debug from 'debug'
import http from 'http'

async function run() {
  const { app } = await createApp()

  /** Get port from environment and store in Express. */
  const port = normalizePort(process.env.PORT || '3100')
  app.set('port', port)

  /** Create HTTP server. */
  const server = http.createServer(app)

  /** Listen on provided port, on all network interfaces. */
  server.listen(port)
  server.on('error', onError)
  server.on('listening', onListening)

  /** Normalize a port into a number, string, or false. */
  function normalizePort(val: any) {
    const portNormalized = parseInt(val, 10)

    if (isNaN(portNormalized)) {
      // named pipe
      return val
    }

    if (portNormalized >= 0) {
      // port number
      return portNormalized
    }

    return false
  }

  /** Event listener for HTTP server "error" event. */
  function onError(error: any) {
    if (error.syscall !== 'listen') {
      throw error
    }

    const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port

    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        console.error(bind + ' requires elevated privileges')
        process.exit(1)
      case 'EADDRINUSE':
        console.error(bind + ' is already in use')
        process.exit(1)
      default:
        throw error
    }
  }

  /** Event listener for HTTP server "listening" event.  */
  function onListening() {
    const addr: any = server.address()
    const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port
    debug('app:server')('Listening on ' + bind)
  }
}

run()
