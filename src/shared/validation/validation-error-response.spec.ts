import { renderValidationErrors } from './validation-error-response'
import { Request, Response } from 'express'
import { expect } from 'chai'
import 'mocha'

describe('validation-error-response', () => {
  describe('renderValidationErrors', () => {
    it('should call next if no errors detected', () => {
      const handler = renderValidationErrors()
      const items = []
      const req = {} as Request
      const res = {} as Response
      handler(req, res, () => items.push(1))
      expect(items.length).to.equal(1)
    })
  })
})