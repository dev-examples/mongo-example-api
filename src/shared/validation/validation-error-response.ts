import { Request, Response, NextFunction } from 'express'
import { validationResult } from 'express-validator'
// ref https://express-validator.github.io/docs/running-imperatively.html
// validate will run all defined validations for the request
// and send a standard response if valisation fails
function renderValidationErrors() {
  return (req: Request, res: Response, next: NextFunction) => {
    const errors = validationResult(req)
    if (errors.isEmpty()) {
      return next()
    }

    res.status(422).json({ error: 'Validation error', errors: errors.array() })
  }
}

export { renderValidationErrors }
