// https://github.com/microsoft/TypeScript/pull/35998
// https://stackoverflow.com/questions/48011353/how-to-unwrap-type-of-a-promise?rq=1
/** Unwraps Promise resolved value in a thenable */
type ThenArg<T> = T extends PromiseLike<infer U> ? U : T

/** Unwraps return type of async function type */
type AsyncReturnType<T extends (...args: any) => any> = ThenArg<ReturnType<T>>

export { ThenArg, AsyncReturnType }