import express, {Request, Response, NextFunction} from 'express'

const router = express.Router()

router.get('/', (req, res, next) => {
  res.send('api root')
})

export default router
