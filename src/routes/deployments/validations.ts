import { check }  from 'express-validator'

function validateNewDeployment() {
  return [
    check('id').not().exists()
  ]
}

export { validateNewDeployment }
