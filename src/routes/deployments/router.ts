import express, { Request, Response } from 'express'
import { validateNewDeployment } from './validations'

import { renderValidationErrors } from '../../shared/validation/validation-error-response'
import { DeploymentsDao } from '../../database/data-access-objects/deployments-dao'

interface DeploymentsRouterOptions {
  deploymentsDao: DeploymentsDao
}

function createDeploymentsRouter({ deploymentsDao }: DeploymentsRouterOptions) {
  const router = express.Router()

  router.post('/', validateNewDeployment(), renderValidationErrors(), async (
    req: Request,
    res: Response,
  ) => {
    try {
      const deployment = await deploymentsDao.create(req.body)
      res.json(deployment)
    } catch (err) {
      res.status(500).json({ error: err.message })
    }
  })

  router.delete('/:id', async (
    req: Request,
    res: Response,
  ) => {
    try {
      await deploymentsDao.deleteById(req.params.id)
      res.send()
    } catch (err) {
      res.status(500).json({ error: err.message })
    }
  })

  router.get('/:id', async (
    req: Request,
    res: Response,
  ) => {
    try {
      const deployment = await deploymentsDao.findById(req.params.id)
      res.json(deployment)
    } catch (err) {
      res.status(500).json({ error: err.message })
    }
  })

  router.get('/', async (
    req: Request,
    res: Response,
  ) => {
    try {
      const deployments = await deploymentsDao.find()
      res.json({ deployments })
    } catch (err) {
      res.status(500).json({ error: err.message })
    }
  })

  return router
}


export { createDeploymentsRouter }
