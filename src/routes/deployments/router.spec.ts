import { expect } from 'chai'
import request from 'supertest'
import { createApp } from '../../app'

const getRequestBody = () => ({
  url: "http://test.test",
  templateName: "Required",
  version: "1.1.1",
  deployedAt: new Date().toISOString()
})

describe('Deployments resource', async () => {
  process.env.MONGO_URL = "mongodb://localhost/test"
  const { app, connection } = await createApp()
  after(async () => connection.disconnect())
  const wipe = async () => connection.mongoose.models.Deployment.deleteMany({})
  beforeEach(wipe)
  afterEach(wipe)

  describe('POST deployments', () => {
    it('should return object with id', async () => {
      const requestBody = getRequestBody()
      const res = await request(app).post('/api/deployments').send(requestBody)

      expect(res.status).to.equal(200, res.text)
      const want = { ...requestBody }
      expect(want).to.include(want)
      expect(res.body).to.have.property('id').to.match(/\w{24}/)
    })
  })

  describe('DELETE deployments', () => {
    it('should return status for successful deletion', async () => {
      const res = await request(app).delete('/api/deployments/111111111111111111111111')

      expect(res.status).to.equal(200, res.text)
      expect(res.text).to.equal('')
    })
  })

  describe('GET one from deployments', () => {
    let id: string
    const requestBody = getRequestBody()
    beforeEach(async () => {
      const res = await request(app).post('/api/deployments').send(requestBody)
      id = res.body.id
    })
    afterEach(async () => {
      await request(app).delete(`/api/deployments/${id}`)
    })
    it('should return an object that was saved earlier', async () => {
      const res = await request(app).get(`/api/deployments/${id}`)

      expect(res.status).to.equal(200, res.text)
      expect(res.body).to.include(requestBody)
    })
  })

  describe('GET deployments', async () => {
    const requestBodies = ['0.1.2', '0.9.9'].map((version) => ({ ...getRequestBody(), version }))
    const ids: string[] = []
    beforeEach(async () => {
      requestBodies.forEach(async (body) => {
        const res = await request(app).post('/api/deployments').send(body)
        ids.push(res.body.id)
      })
    })
    it('should respond with empty list of deployments', async () => {
      const res = await request(app).get('/api/deployments')

      expect(res.body).to.have.property('deployments').and.be.an('array').with.lengthOf(2)
      ids.forEach((id, index) => {
        expect(res.body.deployments.find((item: any) => item.id === id)).to.include(requestBodies[index], res.text)
      })
      expect(res.status).to.equal(200)
    })
  })
})
