import createDebug from 'debug'
import _mongoose, { Mongoose } from 'mongoose'

const debug = createDebug('app:database')

interface ConnectionOptions {
    url: string
    /** uses global mongoose instance if not provided */
    mongoose?: Mongoose
}


async function createConnection({ url = 'mongodb://', mongoose = new Mongoose() }: ConnectionOptions) {
    await mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })

    const rawConnection = mongoose.connection
    rawConnection.on('error', (err) => debug('connection error:', err))
    rawConnection.once('open', () => debug('connection open'))

    const disconnect = () => mongoose.disconnect()

    return { disconnect, mongoose }
}

export { createConnection }
