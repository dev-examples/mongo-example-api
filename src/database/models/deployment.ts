import { Mongoose, Schema } from 'mongoose'
import validator from 'validator'
import { toJSON } from '../../shared/transform/model-transform'

// url: string with url
// templateName: string with template name
// version: string with semantic version
// deployedAt: date when it was added
const deploymentSchema = new Schema({
    url: { type: String, required: true, validate: [value => validator.isURL(value), 'Not a valid URL'] },
    templateName: { type: String, required: true },
    version: { type: String, validate: [/\d+.\d+.\d+.*/, 'Invalid symantic version string'] },
    deployedAt: { type: Date, required: true },
}, { toJSON })

const getDeploymentModel = (mongoose: Mongoose) => mongoose.models.Deployment || mongoose.model('Deployment', deploymentSchema)

export { deploymentSchema, getDeploymentModel }
