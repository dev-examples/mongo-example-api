import { getDeploymentModel } from './deployment'
import { expect } from 'chai'
import { Mongoose } from 'mongoose'

const Deployment = getDeploymentModel(new Mongoose())
/** Testing model without db connection */
describe('Deployment Model', () => {
    describe('Creation', () => {
        it('should create a new model', async () => {
            const testData = { url: 'abc', templateName: '', version: '1', deployedAt: new Date(), nonspec: 1 }
            const deployment = new Deployment(testData)
            expect(deployment.url).to.equal(testData.url)
            expect(deployment.templateName).to.equal(testData.templateName)
            expect(deployment.version).to.equal(testData.version)
            expect(deployment.deployedAt).to.equal(testData.deployedAt)
            expect(deployment).to.not.have.property('test')
        })
        it('should fail validation when invalid data provided', async () => {
            const testData = { url: 'abc', templateName: '', version: '1', deployedAt: new Date() }
            const deployment = new Deployment(testData)
            const error = await deployment.validate().catch((e: any) => e)
            expect(error).to.have.property('message').to.match(/validation failed/)
        })
    })
})