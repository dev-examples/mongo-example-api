import { createConnection } from '../database'
import { AsyncReturnType } from '../../shared/types/awaited'
import { getDeploymentModel } from '../models/deployment'
import { Connection } from 'mongoose'

interface DeploymentsDaoOptions {
    connection: AsyncReturnType<typeof createConnection>
}

/** Abstraction over database library */
async function createDeploymentsDao({ connection }: DeploymentsDaoOptions) {
    const Deployment = getDeploymentModel(connection.mongoose)

    return {
        create: async (props: any) => {
            const deployment = new Deployment(props)
            await deployment.save()
            return deployment
        },
        deleteById: async (id: string) => {
            return Deployment.findByIdAndDelete(id)
        },
        find: async () => {
            return Deployment.find()
        },
        findById: async (id: string) => {
            return Deployment.findById(id)
        },
    }
}

type DeploymentsDao = AsyncReturnType<typeof createDeploymentsDao>

export { createDeploymentsDao, DeploymentsDao }
