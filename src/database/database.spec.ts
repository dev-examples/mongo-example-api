import { createConnection } from './database'
import { expect } from 'chai'
import _mongoose, { Mongoose } from 'mongoose'

describe('Database', () => {
    describe('createConnection', () => {
        const mongoose = new Mongoose()
        after(async () => {
            // await mongoose.connection.close()
            await mongoose.disconnect()
        })
        it('should return a connection', async () => {
            const connection = await createConnection({ url: 'mongodb://localhost/test', mongoose })
            expect(connection).to.have.property('disconnect')
        })
    })
})
